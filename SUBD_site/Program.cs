﻿using System;
using System.IO;

namespace SUBD_site
{
    class Program
    {
        struct Site
        {
            public static int CURRENT_ID = 0;

            public int Id;
            public string UrlAdress;
            public int NumberOfVisits;
            public int AgeLimit;
            public double Raiting;
        }

        static void ResizeArray(ref Site[] sites, int newLength)
        {
            int minLength = newLength > sites.Length ? sites.Length : newLength;

            Site[] newArray = new Site[newLength];

            for (int i = 0; i < minLength; i++)
            {
                newArray[i] = sites[i];
            }

            sites = newArray;
        }

        static int InputInt(string message)
        {
            bool inputResult;
            int number;

            do
            {
                Console.Write(message);
                inputResult = int.TryParse(Console.ReadLine(), out number);
            } while (!inputResult || number < 0);

            return number;
        }

        static string InputString(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        static double InputDouble(string message)
        {
            bool inputResult;
            double numberDouble;

            do
            {
                Console.Write(message);
                inputResult = double.TryParse(Console.ReadLine(), out numberDouble);
            } while (!inputResult || numberDouble < 0);

            return numberDouble;
        }

        static bool InputBool(string message)
        {
            bool inputResult;
            bool b;

            do
            {
                Console.Write(message);
                inputResult = bool.TryParse(Console.ReadLine(), out b);
            } while (!inputResult);

            return b;
        }

        static void AddNewSite(ref Site[] sites, Site site)
        {
            if (sites == null)
            {
                sites = new Site[1];
            }
            else
            {
                ResizeArray(ref sites, sites.Length + 1);
            }

            sites[sites.Length - 1] = site;
        }

        static Site CreateSite(bool isNewId)
        {
            Site site;

            if (isNewId)
            {
                Site.CURRENT_ID++;
                site.Id = Site.CURRENT_ID;
            }
            else
            {
                site.Id = 0;
            }

            site.UrlAdress = InputString("Введите url адрес: ");
            site.NumberOfVisits = InputInt("Введите число посетителей за месяц: ");
            site.AgeLimit = InputInt("Введите возрастное ограничение: ");
            site.Raiting = InputDouble("Введите рейтинг: ");

            return site;
        }

        static void PrintSite(Site site)
        {
            Console.WriteLine("{0,-3}{1,-20}{2,-15}{3,-5}{4,-4}", site.Id, site.UrlAdress, site.NumberOfVisits, site.AgeLimit,
                site.Raiting);
        }

        static void PrintAllSites(Site[] sites)
        {
            Console.WriteLine("{0,-3}{1,-20}{2,-15}{3,-5}{4,-4}", "ИД", "url Адрес", "Кол-во просм", "Огр", "Рейт");

            if (sites == null)
            {
                Console.WriteLine("Массив пуст");
            }
            else if (sites.Length == 0)
            {
                Console.WriteLine("Массив пуст");
            }
            else
            {
                for (int i = 0; i < sites.Length; i++)
                {
                    PrintSite(sites[i]);
                }
            }

            Console.WriteLine("****************");

        }

        static void PrintMenu()
        {
            Console.WriteLine("1. Добавить сайт: ");
            Console.WriteLine("2. Сохранение в файл");
            Console.WriteLine("3. Загрузка из файла");
            Console.WriteLine("4. Сортировка сайтов по кол-ву посещений");
            Console.WriteLine("5. Поиск сайтов с мин по макс возрастным ограничением");
            Console.WriteLine("0. Выход");
        }

        static void SaveAllSitesToFile(Site[] sites, string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);
            writer.WriteLine(sites.Length);
            writer.WriteLine(Site.CURRENT_ID);

            for (int i = 0; i < sites.Length; i++)
            {
                writer.WriteLine(sites[i].Id);
                writer.WriteLine(sites[i].UrlAdress);
                writer.WriteLine(sites[i].NumberOfVisits);
                writer.WriteLine(sites[i].AgeLimit);
                writer.WriteLine(sites[i].Raiting);
            }
            writer.Close();
        }

        static Site[] LoadAllSitesFromFile(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            int countSites = int.Parse(reader.ReadLine());
            Site.CURRENT_ID = int.Parse(reader.ReadLine());

            Site[] sites = new Site[countSites];
            for (int i = 0; i < sites.Length; i++)
            {
                sites[i].Id = int.Parse(reader.ReadLine());
                sites[i].UrlAdress = reader.ReadLine();
                sites[i].NumberOfVisits = int.Parse(reader.ReadLine());
                sites[i].AgeLimit = int.Parse(reader.ReadLine());
                sites[i].Raiting = double.Parse(reader.ReadLine());
            }

            reader.Close();
            return sites;
        }

        static void SortSitesByNumberOfVisits(Site[] sites, bool asc)
        {
            Site temp;
            bool sort;
            int offset = 0;

            do
            {
                sort = true;

                for (int i = 0; i < sites.Length - 1 - offset; i++)
                {
                    bool compareResult;

                    if (asc)
                    {
                        compareResult = sites[i + 1].NumberOfVisits < sites[i].NumberOfVisits;
                    }

                    else
                    {
                        compareResult = sites[i + 1].NumberOfVisits > sites[i].NumberOfVisits;
                    }

                    if (compareResult)
                    {
                        temp = sites[i];
                        sites[i] = sites[i + 1];
                        sites[i + 1] = temp;
                        sort = false;
                    }
                }
                offset++;
            } while (!sort);
        }

        static Site[] FindSitesFromMinToMaxAgeLimit(Site[] sites, int minAgeLimit, int maxAgeLimit)
        {
            Site[] findedSites = null;

            for (int i = 0; i < sites.Length; i++)
            {
                if (sites[i].AgeLimit >= minAgeLimit && sites[i].AgeLimit <= maxAgeLimit)
                {
                    AddNewSite(ref findedSites, sites[i]);
                }
            }

            return findedSites;
        }

        static int GetMinAgeLimit(Site[] sites)
        {
            int minAgeLimit = sites[0].AgeLimit;

            for (int i = 0; i < sites.Length; i++)
            {
                if (sites[i].AgeLimit < minAgeLimit)
                {
                    minAgeLimit = sites[i].AgeLimit;
                }
            }

            return minAgeLimit;
        }

        static int GetMaxAgeLimit(Site[] sites)
        {
            int maxAgeLimit = sites[0].AgeLimit;

            for (int i = 0; i < sites.Length; i++)
            {
                if (sites[i].AgeLimit > maxAgeLimit)
                {
                    maxAgeLimit = sites[i].AgeLimit;
                }
            }

            return maxAgeLimit;
        }

        static void Main(string[] args)
        {

            Site[] sites = null;
            bool runProgram = true;

            while (runProgram)
            {
                Console.Clear();
                PrintAllSites(sites);

                PrintMenu();
                int menuPoint = InputInt("Введите пункт меню: ");

                switch (menuPoint)
                {
                    case 1:
                        {
                            Site site = CreateSite(true);
                            AddNewSite(ref sites, site);
                            continue;
                        }
                        break;

                    case 2:
                        {
                            string fileName = InputString("Введите имя файла для сохранения: ");
                            SaveAllSitesToFile(sites, fileName);

                        }
                        break;

                    case 3:
                        {
                            string fileName = InputString("Введите имя файла для загрузки: ");
                            sites = LoadAllSitesFromFile(fileName);

                        }
                        break;

                    case 4:
                        {
                            bool asc = InputBool("Сортировка по возрастанию? введите(true/false)");
                            SortSitesByNumberOfVisits(sites, asc);
                        }
                        break;

                    case 5:
                        {
                            Console.WriteLine("Введите ограничение по возрасту в диапазоне от " + $"{GetMinAgeLimit(sites)} до {GetMaxAgeLimit(sites)}");

                            int minAgeLimit = InputInt("Минимальное возрастное ограничение:");

                            int maxAgeLimit = InputInt("Максимальное возрастное ограничение:");

                            Site[] findedSites = FindSitesFromMinToMaxAgeLimit(sites, minAgeLimit, maxAgeLimit);

                            PrintAllSites(findedSites);
                        }
                        break;


                    case 0:
                        {
                            Console.WriteLine("Программа будет завершена");

                            runProgram = false;
                        }
                        break;

                    default:
                        {
                            Console.WriteLine("Неверный пункт меню");
                        }
                        break;
                }

                Console.ReadKey();
            }
        }
    }
}
